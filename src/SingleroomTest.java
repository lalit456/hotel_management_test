import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SingleroomTest {
    private int num;
    private static Singleroom Obj1;
    @Test
    public void InputValidations() {
//        Obj1 = new Singleroom(null,null,null);
//        Obj1 = new Singleroom("asasa","asaas","asda");
        assertNull(Obj1,"Your Entered input is null");

    }
    @Test
    public void InputValidations1() {
        Obj1 = new Singleroom("lalit","1234567890","male");
        assertAll();

    }

    @Test
    public void InputTestCase1() {

        String expectedResult = "Rahul" ;

        String actualResult = Obj1.name;

        assertEquals(expectedResult,actualResult,"check name");
    }

    @Test
    public void InputTestCase2() {

        String expectedResult = "0141256321" ;

        String actualResult = Obj1.contact;

        assertTrue(expectedResult==actualResult,"the input value is true");
    }

    @Test
    public void InputTestCase3() {

        String expectedResult = "Male" ;

        String actualResult = Obj1.gender;

        assertSame(expectedResult, actualResult, "Checking the gender");
    }
    @Test
    public void TestContactDetails(){
        Singleroom Obj = new Singleroom("Rahul", "1012545630","Male");


        assertAll(
                () -> assertTrue(Obj.contact.length() == 10),
                () -> assertFalse(Obj.contact.length()!= 10,"Enter a valid number"),
                () ->  assertNotEquals(Obj.contact.getClass().getSimpleName(), ((Object) num).getClass().getSimpleName(), "Contact type should be number")
        );

    }





}




