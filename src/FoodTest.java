import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class FoodTest {
       private static Food Obj1;
    @BeforeAll
    static void setupAll() {
        Obj1 = new Food(2,5);
    }

    @Test
    public void InputValidations() {
        Food obj1 = new Food(1,2);
        assertFalse(obj1.itemno == 0|| obj1.quantity == 0);

    }
    @Test
    public void InputTesting1(){
            Obj1 = new Food(1,2);
            float expectedResult = 100;

            float actualResult = Obj1.price;
            assertEquals(expectedResult,actualResult);
        }
    @Test
    public void InputTesting2(){
        Obj1 = new Food(2,2);
        float expectedResult = 120;

        float actualResult = Obj1.price;
        assertEquals(expectedResult,actualResult);
    }
    @Test
    public void whenExceptionThrown_thenAssertionSucceeds() {
        Exception exception = assertThrows(NumberFormatException.class, () -> {
            Integer.parseInt(String.valueOf(Obj1));
        });

        String expectedMessage = "For input string";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }



    }








